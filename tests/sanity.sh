#!/bin/bash
#
# This is a simple sanity test to satisfy the RHEL8.1 onboard gating
# requirement.

ret=0
qperf --help examples
let ret=$ret+$?

qperf --help tests
let ret=$ret+$?

# launch server
qperf &

qperf localhost tcp_bw tcp_lat
let ret=$ret+$?

qperf localhost udp_bw udp_lat
let ret=$ret+$?

# kill off server
killall qperf

exit $ret
